from django.urls import include, path
from django.contrib import admin
from django.views.generic.base import RedirectView

from app.urls import router as app_router
from . import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(app_router.urls)),
    path('favicon.ico/', RedirectView.as_view(url=settings.STATIC_URL +
                                              'favicon.ico', permanent=True))
]
